'''\
Start and manage condor DAG jobs

DAG: Directed Acyclic Graph
'''
from . import Job
class Dag(object):
    '''\

    construct, write, and submit dag jobs
    '''
    def __init__(
                 self,
                 project,
                 fname,
                ):
        '''\
        initialize the dag object
        '''
        # Record inputs
        self.nodes = []
        self.pre_args = {}
        self.post_args = {}
        self.pre_scripts = {}
        self.post_scripts = {}
        self.abort_dag = {}
        self.abort_retry = {}
        self.project = project
        self.fname = fname

    def add_node(
                 self,
                 tag,
                 wkdir,
                 exe,
                 retries = None,
                 parents = (),
                 pre_script = None,
                 pre_args = None,
                 post_script = None,
                 post_args = None,
                 abort_dag = False,
                 abort_retry = False,
                **kwargs):
        '''\
        Add a node
        '''
        # Initialize a new job
        node = Job(self.project, wkdir, exe, tag = tag,**kwargs)
        # Update the new node's parents
        node.parents = parents
        # Update the node's retries
        if not (retries is None):
            node.retries = int(retries)
        else:
            node.retries = None
        # Append the node to our list
        self.nodes.append(node)
        # Check prescript
        if not pre_script is None:
            self.pre_scripts[tag] = pre_script
            self.pre_args[tag] = pre_args
        # Check postscript
        if not post_script is None:
            self.post_scripts[tag] = post_script
            self.post_args[tag] = post_args
        # Abort dag
        if abort_dag:
            assert isinstance(abort_dag, int)
            self.abort_dag[tag] = abort_dag
        # Abort retry
        if abort_retry:
            assert isinstance(abort_retry, int)
            self.abort_retry[tag] = abort_retry
  
    def write(self):
        '''\
        write the dag files
        '''
        ### Write all the submit files ###
        for i in range(len(self.nodes)):
            self.nodes[i].write()

        ### Open the dag file ###
        with open(self.fname, 'w') as dag_file:

            ## Introductory stuff ##
            # Write the signature
            dag_file.write("# Automatically generated dag file\n")
            # Identify the project. This doesn't work on the OSG
            #dag_file.write("# Project\n")
            #dag_file.write("+ProjectName = \"%s\"\n"%(self.project))

            ## Write all of the jobs to the dag ##
            for i in range(len(self.nodes)):
                # Select current node
                node = self.nodes[i]
                # Write a line for that node
                dag_file.write(
                               "JOB %s %s DIR %s\n"%(
                                                     node.tag, 
                                                     node.fname_sub, 
                                                     node.wkdir
                                                    )
                              )

            ## Write all the pairings ##
            for i in range(len(self.nodes)):
                # Select current node
                node = self.nodes[i]

                # Check if each node has a pairing
                if node.parents != ():
                    # If it does, we need to construct the parent string
                    parent_string = ""
                    for j in range(len(node.parents)):
                        parent_string = parent_string + node.parents[j] +  " "

                    # Write the relationship
                    dag_file.write("PARENT %s CHILD %s\n"%(parent_string, node.tag))

            ## Write all the retries ##
            for i in range(len(self.nodes)):
                node = self.nodes[i]

                # Check if it has retries
                if not (node.retries is None):
                    # construct a string with the proper syntax for the dag file #
                    retry_string = "RETRY %s %d"%(node.tag, node.retries)
                    # Check abort
                    if node.tag in self.abort_retry:
                        retry_string = retry_string + ' ' + "UNLESS-EXIT %d"%(self.abort_retry[node.tag])
                    # Append newline character
                    retry_string = retry_string + '\n'
                    # Write the line representing the number of retries
                    dag_file.write(retry_string)

            ## Write all the abort retries ##
            for tag in self.abort_dag:
                # Initialize the abort line
                abort_line = "ABORT-DAG-ON %s %d\n"%(tag, self.abort_dag[tag])
                # Write the abort line
                dag_file.write(abort_line)

            ## Write all of the pre-scripts ##
            for tag in self.pre_scripts:
                # Create the pre_script line
                pre_script_line = "SCRIPT PRE %s %s"%(tag, self.pre_scripts[tag])
                # Check the arguments of the pre_script
                if not self.pre_args[tag] is None:
                    pre_script_line = pre_script_line + " %s"%self.pre_args[tag]
                # Add the newline character
                pre_script_line = pre_script_line + '\n'
                # Write the prescript line
                dag_file.write(pre_script_line)

            ## Write all of the post-scripts ##
            for tag in self.post_scripts:
                # Create the post_script line
                post_script_line = "SCRIPT POST %s %s"%(tag, self.post_scripts[tag])
                # Check the arguments of the post_script
                if not self.post_args[tag] is None:
                    post_script_line = post_script_line + " %s"%self.post_args[tag]
                # Add the newline character
                post_script_line = post_script_line + '\n'
                # Write the postscript line
                dag_file.write(post_script_line)



    def submit(self, maxidle=None):
        '''\
        Submit the dag job
        '''
        import os
        # If the os library isn't available, you're probably not working
        # on the OSG, and therefore, this module may not work as intended
        # Prepare the command 
        cmd = "condor_submit_dag"
        if not (maxidle is None):
            cmd += " -maxidle %d"%(maxidle)
        cmd += " %s"%(self.fname)
        # Submit the dag
        os.system(cmd)
