'''\
Start and manage condor jobs
'''
class Job(object):
    '''\
    Job class holds job methods
    '''
    def __init__(
                 self, 
                 project_name,
                 wkdir,
                 exe,
                 cpus = 1,
                 memory = 1,
                 disk = 1,
                 queue = 1,
                 singularity_image = None,
                 input_files = [],
                 output_files = [],
                 output_remaps = [],
                 arguments = "",
                 tag = "job",
                 retries = 0,
                 periodic_release = False,
                ):
        '''\
        Initialize a job
        '''

        # Determine the fnames for tracking
        self.fname_err = wkdir + "/" + tag + ".err"
        self.fname_out = wkdir + "/" + tag + ".out"
        self.fname_log = wkdir + "/" + tag + ".log"
        self.fname_sub = wkdir + "/" + tag + ".sub"

        # General stuff
        self.tag = tag
        self.wkdir = wkdir
        self.project_name = project_name
        self.exe = exe
        self.cpus = cpus
        self.memory = memory*1024 # Gb -> Mb
        self.disk = disk*1024 # Gb -> Mb
        self.queue = queue
        self.singularity_image = singularity_image
        self.input_files = input_files
        self.output_files = output_files
        self.output_remaps = output_remaps
        self.arguments = arguments
        self.retries = retries
        self.periodic_release = periodic_release

    def write(self):
        '''\
        Write the submit file
        '''
        requirements = []
        # keep track of input and output files
        input_files = self.input_files.copy()
        output_files = self.output_files.copy()
        output_remaps = self.output_remaps.copy()
        # Avoid tempest
        requirements.append('GLIDEIN_Site =!= "MTState-Tempest"')

        # Open and write the sub file
        with open(self.fname_sub, 'w') as submit:
            # Write signature comment in file
            submit.write("# Autmatically generated job submit file\n")

            # Write the project name for submission
            submit.write("\n# Project name\n")
            submit.write("+ProjectName = \"%s\"\n"%(self.project_name))

            # Write singularity information
            if not self.singularity_image is None:
                # Use singularity, comment as such
                submit.write("\n# Singularity information:\n")
                requirements.append("HAS_SINGULARITY == TRUE")
                # Point to the singularity image
                submit.write("+SingularityImage = \"%s\"\n"%(self.singularity_image))
            else:
                # Otherwise, this is just a test, so use the default universe
                submit.write("universe = vanilla\n")

            # Don't retry on the same node you failed on
            # Thanks Richard!
            if not(self.retries is None):
                for i in range(self.retries):
                    requirements.append(
                        "TARGET.GLIDEIN_ResourceName =!= MY.MachineAttrGLIDEIN_ResourceName%d"%(i+1))

            # Write the requirements if there are any
            if not (requirements is []):

                # Begin requirements string
                req_str = "Requirements = "

                # For each requirement...
                for i in range(len(requirements)):
                    # add that requirement
                    req_str += requirements[i]
                    # If it's not the last one, add the AND operator
                    if i != len(requirements) - 1:
                        req_str += " && \ \n"
                    # If it IS the last one, just end the line
                    else:
                        req_str += "\n"

            # Write the requirements
            submit.write(req_str)

            # Write periodic release
            if self.periodic_release:
                assert isinstance(self.periodic_release, str)
                submit.write("periodic_release = %s\n"%self.periodic_release)

            # Write the location of the executable
            submit.write("\n# Location of the executable\n")
            submit.write("executable = %s\n"%(self.exe))

            # Write log file locations
            submit.write("\n# Log files:\n")
            submit.write("error = %s\n"%(     self.fname_err))
            submit.write("output = %s\n"%(    self.fname_out))
            submit.write("log = %s\n"%(       self.fname_log))

            # Write input files
            if len(input_files) > 0:
                # Write each input file
                submit.write("\n# Input files\n")
                file_string = ""
                for i in range(len(input_files)):
                    file_string = file_string + input_files[i] + ","
                submit.write("transfer_input_files = %s\n"%(file_string))
            
            # Make sure you transfer output files at the right time
            submit.write("ShouldTransferFiles = True\n")
            submit.write("WhenToTransferOutput = ON_EXIT\n")
            
            # Handle stdout and stderr
            # If transfer output files is enabled,
            # and stdout and stderr are not included in that list
            # Then they won't be returned
            # This might be a bug with HTCondor
            stdout = "_condor_stdout = %s"%(self.fname_out.split('/')[-1])
            output_remaps.append(stdout)
            stderr = "_condor_stderr = %s"%(self.fname_err.split('/')[-1])
            output_remaps.append(stderr)

            # Write output files
            if len(output_files) > 0:
                # Write each output file
                submit.write("\n# Output files\n")
                file_string = ""
                for i in range(len(output_files)):
                    file_string = file_string + output_files[i] + ","
                submit.write("transfer_output_files = %s\n"%(file_string))

            # Write output remaps
            if len(output_remaps) > 0:
                # Yes, right now this is an "if True" statmenet
                submit.write("\n# Output files (HOST = LOCAL)\n")
                file_string = ""
                for i in range(len(output_remaps)):
                    file_string = file_string + "%s;"%(output_remaps[i])
                submit.write("transfer_output_remaps = \"%s\"\n"%(file_string))

            # Arguments
            if not self.arguments is "":
                # Write arguments
                submit.write("\n# Arguments:\n")
                submit.write("arguments = %s\n"%(self.arguments))

            # Request Resources
            submit.write("\n# Resources:\n")
            submit.write("request_cpus = %d\n"%(self.cpus))
            submit.write("request_memory = %d MB\n"%(self.memory))
            submit.write("request_disk = %d MB\n"%(self.disk))

            # Write the number of these jobs to run
            # I don't really do this, so it hasn't been tested very well
            submit.write("\n# Number of identical jobs to run: \n")
            submit.write("queue %d\n"%(self.queue))

    def submit(self):
        '''\
        Submit the job
        '''
        # Dan will probably suggest a slightly different approach, 
        # besides using os, but condor stuff is highly machine specific
        # and the Open Science Grid submit nodes can use os.
        # So if I wanted to update this code to accomidate that,
        # I would probably need to do a lot more research on HTCondor first.
        import os
        import subprocess

        # Change working directory
        os.chdir(self.wkdir)

        # Write command as tuple
        command = ("condor_submit", self.fname_sub)
        # Execute command
        cmd_output = subprocess.check_output(command)

        # Pull the cluster number out of the submit file
        string_output = str(cmd_output)
        print(string_output)
        list_output = string_output.split()
        cluster_string = list_output[6].split('.')[0]
        cluster_int = int(cluster_string)
        self.cluster_id = cluster_int
