'''\
Contains condor tools such as read log

tools.py

Vera Del Favero
'''
def log_time(time_line):
    '''\
    Simple time converter
    Input: line from log file, containing time
    Return: the time in seconds past the epoch
    '''
    import time
    # split the line into a list
    line = time_line.split()
    # Extract the date and time strings
    date = line[2]
    stringtime = line[3]
    # Extract the necessary time information
    yr, mo, dy = date.split('-')
    yr, mo, dy = int(yr), int(mo), int(dy)
    hr, mi, sc = stringtime.split(":")
    hr, mi, sc = int(hr), int(mi), int(sc)
    # Create a time struct
    structtime = time.struct_time(
                    (yr, mo, dy, hr, mi, sc, -1, -1, -1))
    # Convert to seconds past the epoch
    t = time.mktime(structtime)
    return t


def read_log(fname_log):
    '''\
    Read a log file for an existing job,
        and record useful quantities
    '''
    import time
    # Initiate variables
    run_bytes_sent = 0
    run_bytes_received = 0
    total_bytes_sent = 0
    total_bytes_received = 0
    cpus_usage = 0.0
    cpus_request = 0.0
    cpus_allocated = 0.0
    disk_units = ""
    disk_usage = 0.0
    disk_request = 0.0
    disk_allocated = 0.0
    memory_units = ""
    memory_usage = 0.0
    memory_request = 0.0
    memory_allocated = 0.0
    t_start = 0.0
    t_end = 0.0

    print("Opening %s"%(fname_log))
    with open(fname_log, 'r') as log:
        # Read through each line
        for line in log:
            if "Job submitted from host" in line:
                t_start = log_time(line)
            elif "Job terminated." in line:
                t_end = log_time(line)
            elif "Run Bytes Sent By Job" in line:
                run_bytes_sent = int(line.split()[0])
            elif "Run Bytes Received By Job" in line:
                run_bytes_received = int(line.split()[0])
            elif "Total Bytes Sent By Job" in line:
                total_bytes_sent = int(line.split()[0])
            elif "Total Bytes Received By Job" in line:
                total_bytes_received = int(line.split()[0])
            elif "Cpus" in line:
                line = line.split()
                cpus_usage = float(line[2])
                cpus_request = float(line[3])
                cpus_allocated = float(line[4])
            elif "Disk" in line:
                line = line.split()
                disk_units = line[1].lstrip('(').rstrip(')')
                disk_usage = float(line[3])
                disk_request = float(line[4])
                disk_allocated = float(line[5])
            elif "Memory (" in line:
                line = line.split()
                memory_units = line[1].lstrip('(').rstrip(')')
                memory_usage = float(line[3])
                memory_request = float(line[4])
                memory_allocated = float(line[5])

    if (t_start != 0) and (t_end != 0):
        t_elapsed = int(t_end - t_start)
    else:
        t_elapsed = 0
            
    fields = (
              run_bytes_sent,
              run_bytes_received,
              total_bytes_sent,
              total_bytes_received,
              cpus_usage,
              cpus_request,
              cpus_allocated,
              disk_units,
              disk_usage,
              disk_request,
              disk_allocated,
              memory_units,
              memory_usage,
              memory_request,
              memory_allocated,
              t_start,
              t_end,
              t_elapsed,
             )

    field_names = (
              "run_bytes_sent",
              "run_bytes_received",
              "total_bytes_sent",
              "total_bytes_received",
              "cpus_usage",
              "cpus_request",
              "cpus_allocated",
              "disk_units",
              "disk_usage",
              "disk_request",
              "disk_allocated",
              "memory_units",
              "memory_usage",
              "memory_request",
              "memory_allocated",
              "t_start",
              "t_end",
              "t_elapsed",
             )
    return field_names, fields
