#!/usr/bin/env python3
'''\
Test the job code for python3
'''
import os
from os.path import join, isfile, isdir, expanduser
from xcondor.job import Job
from xcondor.dag import Dag

PROJECT = "CompBinFormMod"
HOME = expanduser("~")
XEV_CONDOR = join(HOME, "Repos", "xev-condor")
TESTS = join(XEV_CONDOR, "tests")
SCRIPTS=join(XEV_CONDOR, "tests", "scripts")
WKDIR = join(XEV_CONDOR, "tests", "test")
singularity_image = "/cvmfs/singularity.opensciencegrid.org/xevra/gwalk:latest"

# Testing something
periodic_release="(((HoldReasonCode == 13) && (HoldReasonSubCode == 256)) || ((HoldReasonCode == 19) && (HoldReasonSubCode == 1)) || ((HoldReasonCode == 12) && (HoldReasonSubCode == 1))) && (NumHolds < 7)"

# Executables
exe     = join(SCRIPTS, "myscript.sh")
exe_A   = join(SCRIPTS, "plan_01.sh")
exe_B   = join(SCRIPTS, "plan_02.sh")

# Clear directory
os.system("rm -f %s/*"%WKDIR)


#input_files = ["/home/xevra/condor/setup.py"]
mydag = Dag(PROJECT, join(WKDIR, "mydag.dag"))

mydag.add_node(
               "myscript",
               WKDIR,
               exe,
               singularity_image = singularity_image,
               output_files = ["results.dat"],
               retries = 7,
               periodic_release=periodic_release,
               #pre_script=join(SCRIPTS, "hello.sh"),
               post_script=join(SCRIPTS, "fail.sh"),
               #pre_script=join(SCRIPTS, "echo.sh"),
               #pre_args="I like green eggs and ham",
               #post_script=join(SCRIPTS, "echo.sh"),
               #post_args="My name is Sam I am",
               #abort_dag=1,
               abort_retry=1,
              )


mydag.add_node(
               "step_1",
               WKDIR,
               exe_A,
               singularity_image = singularity_image,
               output_files = ["secret_message.txt.gz"],
               retries = 4,
               periodic_release=periodic_release,
              )

mydag.add_node(
               "step_2",
               WKDIR,
               exe_B,
               singularity_image = singularity_image,
               input_files = ["secret_message.txt.gz"],
               output_files = ["secret_message.txt"],
               retries = 1,
               periodic_release=periodic_release,
               parents = ("step_1", ),
              )


mydag.write()

#mydag.submit()
