#!/bin/bash
# Count the number of instances for device or resource busy errors
out=$(grep "Device or resource busy" ${1} | wc -l)
# Check if there are more than zero
if (( "$out" > 0 )); then
    # There are more than zero. Die.
    echo "ERROR"
    exit 1
fi
# There are not more than zero. Exit
echo "No problem"
exit 0

